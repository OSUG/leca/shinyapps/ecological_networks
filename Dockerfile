FROM gricad-registry.univ-grenoble-alpes.fr/osug/dc/shiny-base:4.0.2

# Install dependencies and clean tmp files
RUN install.r \
      igraph \
      visNetwork \ 
      RCurl \
      dplyr\
      && rm -rf /tmp/downloaded_packages

# Copy app files to /srv/shiny
COPY app.R meta_vertebrates_europe.Rdata nodes_info_vert.csv meta_angola.Rdata SBMgroups_spp.csv /srv/shiny/
COPY meta_globnets.Rdata /srv/shiny/
COPY app.R coords_vert.Rdata coords_angola.Rdata /srv/shiny/
COPY silouhette_metaweb_europe /srv/shiny/silouhette_metaweb_europe

