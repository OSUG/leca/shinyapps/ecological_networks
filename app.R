library(RCurl)
library(igraph)
library(visNetwork)
library(dplyr)
# setwd("~/Bureau/expo_confluence/ecological_networks/")

get_vertebrate_network <- function(beta = 0.005,x_y_range = c(5,1)){
  load("meta_vertebrates_europe.Rdata")
  load('coords_vert.Rdata')
  nodes_table = read.table("SBMgroups_spp.csv",sep = ";",header = T,stringsAsFactors = F)
  nodes_info = data.frame(id = as.character(1:length(unique(nodes_table$groups)))) 
  nodes_info =  mutate(nodes_info,info = apply(nodes_info,1,function(x) paste(nodes_table$species[which(nodes_table$groups == x)],collapse = "\n")))
  
  #add images vismetaNetwork <- function(g = NULL,metanetwork,legend = NULL,
  #get resources and consumers
  x_y_range = c(1.5,1.5)
  
  metanetwork = meta0
  mode = "vert"
  g = meta0$metaweb_group
  #edge_thrs
  A = get.adjacency(g,attr = "weight") %>% as.matrix()
  A[which(A<0.4)] = 0
  g = graph_from_adjacency_matrix(A,mode = "directed",weighted = T)
  #to get image in local (cf https://github.com/datastorm-open/visNetwork/issues/65)
  txt = c()
  for(p in V(g)$name){
    txt = c(txt,
              RCurl::base64Encode(readBin(paste0('silouhette_metaweb_europe/',p,'.png'), 
                                        'raw', 
                                        file.info(paste0('silouhette_metaweb_europe/',p,'.png'))[1, 'size']), 
                                'txt')
    )
  }
  
  nodes = data.frame(id = igraph::V(g)$name,
                     shape = rep("image",vcount(g)),
                     image = paste('data:image/png;base64', txt, sep = ','),
                     info = nodes_info[igraph::V(g)$name,"info"],
                     stringsAsFactors = F)
  edges = data.frame(igraph::get.edgelist(g),
                     arrows = c("to"),
                     width = E(g)$weight)
  colnames(edges) = c('from','to','arrows','width')
  
  network_loc = visNetwork::visNetwork(nodes, edges, height = "1000px", width = "100%") %>%
    #visNodes(shape = "square") %>%  # square for all nodes
    #visNetwork::visNodes(shapeProperties = list(useBorderWithImage = TRUE)) %>% 
    visNetwork::visOptions(highlightNearest = TRUE) %>%
    customLayout(metanetwork = metanetwork,g = g,mode = mode,beta = beta,x_y_range = x_y_range) %>%
    visNetwork::visEvents(selectNode = "function(properties) {
      alert(this.body.data.nodes.get(properties.nodes[0]).info) ;}") 
  return(network_loc)
}

#custom layout function for visnetwork
customLayout <- function(graph,g,metanetwork,mode,beta,x_y_range){
  igraphlayout = list(type = "square")
  # g = graph_from_data_frame(graph$x$edges)
  n = igraph::vcount(g)
  # if(mode == 'TL-kpco'){
  #   coords = get_nodes_position_TL_kpco(g,TL_loc,beta = beta)
  # }else if(mode == 'TL-tsne'){
  #   coords = get_nodes_position_TL_tsne(g,TL_loc,beta = beta,TL_tsne.config = TL_tsne.default)
  # }
  #using precomputed coordinates
  if(mode == "vert"){
    load("meta_vertebrates_europe.Rdata")
    coords = cbind(V(meta0$metaweb_group)$layout_beta0.004,max(V(meta0$metaweb_group)$TL)-V(meta0$metaweb_group)$TL)
  }
  if(mode == "angola"){
    load("coords_angola.Rdata")
    coords = coords_angola
  }
  if(mode == "globnets"){
    load("meta_globnets.Rdata")
    coords = cbind(V(meta_globnets$metaweb)$TL,V(meta_globnets$metaweb)$layout_beta0.007)
  }
  rownames(coords) = V(g)$name
  graph$x$nodes$x = coords[graph$x$nodes$id, 1]*100*x_y_range[1]
  graph$x$nodes$y = coords[graph$x$nodes$id, 2]*1000*x_y_range[2]
  message(paste0('x_max = ',max(graph$x$nodes$x)))
  message(paste0('y_max = ',max(graph$x$nodes$y)))
  graph$x$options$layout = igraphlayout
  graph %>% visNetwork::visNodes(physics = FALSE) %>% visNetwork::visEdges(smooth = F,shadow = F)
}


vismetaNetwork <- function(g = NULL,metanetwork,legend = NULL,
                           mode = 'TL-kpco',beta = 0.1,x_y_range = c(1,1)){
  if(is.null(g)){
    g = metanetwork$metaweb
  } 
  if(is.null(legend)){
    #get resources and consumers
    resources_list = sapply(igraph::V(g)$name,
                            function(x) igraph::neighbors(g,x,mode = 'in')$name)
    resources_list[which(lapply(resources_list,length)==0)] = ""
    consumers_list = sapply(igraph::V(g)$name,
                            function(x) igraph::neighbors(g,x,mode = 'out')$name)
    consumers_list[which(lapply(consumers_list,length)==0)] = "" 
    
    nodes = data.frame(id = igraph::V(g)$name, value = igraph::V(g)$ab,
                       resources = paste("\n",unname(unlist(lapply(
                         resources_list,
                         paste,collapse = ', '))),"\n"),
                       consumers = paste("\n",unname(unlist(lapply(
                         consumers_list,
                         paste,collapse = ', '))),"\n"),
                       TL = paste('\n Trophic level',round(V(g)$TL,digits = 3)),
                       width = ifelse(is.null(E(g)$weight),1,E(g)$weight),
                       stringsAsFactors = F)
    edges = data.frame(igraph::get.edgelist(g),
                       arrows = c("to"),
                       width = E(g)$weight)
    colnames(edges) = c('from','to','arrows','width')
    
    network_loc = visNetwork::visNetwork(nodes, edges, height = "1000px", width = "100%") %>%
      #visNodes(shape = "square") %>%  # square for all nodes
      visNetwork::visNodes(shapeProperties = list(useBorderWithImage = TRUE)) %>% 
      visNetwork::visOptions(highlightNearest = TRUE) %>%
      customLayout(metanetwork = metanetwork,g = g,mode = mode,beta = beta,x_y_range = x_y_range) %>%
      visNetwork::visEvents(selectNode = "function(properties) {
      alert(this.body.data.nodes.get(properties.nodes[0]).id + 
              ' se nourrissent de: ' + this.body.data.nodes.get(properties.nodes[0]).resources +
              this.body.data.nodes.get(properties.nodes[0]).id + 
              'sont consommé(e)s par: ' + this.body.data.nodes.get(properties.nodes[0]).consumers +
              this.body.data.nodes.get(properties.nodes[0]).TL);}")  %>%
      visNetwork::visLegend()
    return(network_loc)
  } else{
    # get the resolution of g
    res_local = g$res
    #legend must be one of the available resolution
    if(!(legend %in% colnames(metanetwork$trophicTable))){
      if(is.null(metanetwork$trophicTable)){
        stop('single resolution available, legend is not possible')
      }else{
        stop(paste0('legend must be one of the available resolutions : ',
                    Reduce(paste,colnames(metanetwork$trophicTable))))
      }
    }
    if(!(which(colnames(metanetwork$trophicTable) == res_local) <
         which(colnames(metanetwork$trophicTable) == legend))){
      stop("legend must be a coarser resolution than resolution of the current network")
    }else{
      #get resources and consumers
      resources_list = sapply(igraph::V(g)$name,
                              function(x) igraph::neighbors(g,x,mode = 'in')$name)
      resources_list[which(lapply(resources_list,length)==0)] = ""
      consumers_list = sapply(igraph::V(g)$name,
                              function(x) igraph::neighbors(g,x,mode = 'out')$name)
      consumers_list[which(lapply(consumers_list,length)==0)] = "" 
      
      nodes = data.frame(id = igraph::V(g)$name, value = igraph::V(g)$ab,
                         group = metanetwork$trophicTable[igraph::V(g)$name,legend],
                         resources = paste("\n",unname(unlist(lapply(
                           resources_list,
                           paste,collapse = ', '))),"\n"),
                         consumers = paste("\n",unname(unlist(lapply(
                           consumers_list,
                           paste,collapse = ', '))),"\n"),
                         TL = paste('\n Niveau trophique :',round(V(g)$TL,digits = 3)),
                         stringsAsFactors = F)
      
      edges = data.frame(igraph::get.edgelist(g),
                         # arrows
                         arrows = c("to"),
                         width = E(g)$weight)
      colnames(edges) = c('from','to','arrows','width')
      
      network_loc = visNetwork::visNetwork(nodes, edges, height = "1000px", width = "100%") %>%
        #visNodes(shape = "square") %>%  # square for all nodes
        visNetwork::visNodes(shapeProperties = list(useBorderWithImage = TRUE)) %>% # images
        visNetwork::visOptions(highlightNearest = TRUE) %>%
        customLayout(metanetwork = metanetwork,g = g,mode = mode,beta = beta,x_y_range = x_y_range) %>%
        visNetwork::visEvents(selectNode = "function(properties) {
      alert(this.body.data.nodes.get(properties.nodes[0]).id + 
              ' consumes ' + this.body.data.nodes.get(properties.nodes[0]).resources +
              this.body.data.nodes.get(properties.nodes[0]).id + 
              ' is consumed by ' + this.body.data.nodes.get(properties.nodes[0]).consumers +
              this.body.data.nodes.get(properties.nodes[0]).TL);}")  %>%
        visNetwork::visOptions(selectedBy = "group",
                               highlightNearest = TRUE,
                               nodesIdSelection = TRUE)  %>%
        visNetwork::visLegend()
      return(network_loc)
    }
  }
}



server <- function(input, output) {
  output$mynetworkid1 <- renderVisNetwork({
    #european vertebrate network
    get_vertebrate_network()
  })
  output$mynetworkid2 <- renderVisNetwork({
    # angola_trophicTable = read.table("angola_trophicTable.csv",header = T,sep =',',
    #                                  stringsAsFactors = F,row.names = 1)
    # angola_trophicTable$Species = rownames(angola_trophicTable)
    # meta_angola_french = meta_angola
    # metaweb_angola_french = meta_angola$metaweb
    # meta_angola_french$trophicTable = angola_trophicTable
    # metaweb_angola_french = permute(metaweb_angola_french,order(order(V(metaweb_angola_french)$name)))
    # V(metaweb_angola_french)$name = rownames(angola_trophicTable)
    # meta_angola_french = build_metanetwork(metaweb = metaweb_angola_french, trophicTable = angola_trophicTable)
    # meta_angola_french = compute_trophic_levels(meta_angola_french)
    # save(meta_angola_french,file = "meta_angola_french.Rdata")
    # coords_angola = get_nodes_position_TL_tsne(g = meta_angola_french$metaweb,TL = V(meta_angola_french$metaweb)$TL,beta = 0.3,
    #                            TL_tsne.config = TL_tsne.default)
    # save(coords_angola,file = 'coords_angola.Rdata')

    load('meta_angola.Rdata')
    # angola coastal network
    vismetaNetwork(metanetwork = meta_angola,mode = 'angola',beta = 0.3,
                   legend = 'Phylum',x_y_range = c(6,0.007)) %>%
      visNetwork::visLegend(width = 0.1, 
                            position = "right")
  })
  output$mynetworkid3 <- renderVisNetwork({
    
    # load('meta_globnets.Rdata')
    # globnets_trophicTable = read.table("globnets_trophicTable.csv",header = T,sep =',',stringsAsFactors = F)
    # globnets_trophicTable$trophic_group = rownames(globnets_trophicTable)
    # meta_globnets_french = meta_globnets
    # meta_globnets_french$trophicTable = globnets_trophicTable
    # metaweb_globnets_french = meta_globnets_french$metaweb
    # metaweb_globnets_french = permute(metaweb_globnets_french,order(order(V(metaweb_globnets_french)$name)))
    # V(metaweb_globnets_french)$name = rownames(globnets_trophicTable)
    # meta_globnets_french = build_metanetwork(metaweb = metaweb_globnets_french, trophicTable = globnets_trophicTable)
    # meta_globnets_french = compute_trophic_levels(meta_globnets_french)
    # save(meta_globnets_french,file = "meta_globnets_french.Rdata")
    # coords_globnets = get_nodes_position_TL_tsne(g = meta_globnets_french$metaweb,TL = V(meta_globnets_french$metaweb)$TL,beta = 0.005,
    #                            TL_tsne.config = TL_tsne.default)
    # save(coords_globnets,file = 'coords_globnets.Rdata')
    load('meta_globnets.Rdata')

    vismetaNetwork(metanetwork = meta_globnets,mode = 'globnets',legend = "taxa",
                   beta = 0.007,x_y_range = c(8,0.04)) %>%
      visNetwork::visLegend(width = 0.1, 
                            position = "right") %>%
      visNodes(size = 10)
  })
}
ui <- fillPage(
  title = "Trophic networks from different ecosystems",
  navbarPage("Trophic networks from different ecosystems",
             tabPanel("European vertebrates metaweb",fluidRow(
               column(12, visNetworkOutput("mynetworkid1",height=1000)
               ))),
             tabPanel("Angola coastal network",fluidRow(
               column(12, visNetworkOutput("mynetworkid2",height=1000)
               ))),
             tabPanel("Norway soil network",fluidRow(
               column(12, visNetworkOutput("mynetworkid3",height=1000)
               )))
  )
)
shinyApp(ui = ui, server = server)